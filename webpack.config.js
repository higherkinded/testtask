var path = require('path');

var join = path.join;

var PATHS = {
  src: path.resolve('src'),
  public: path.resolve('public'),
};

module.exports = {
  mode: 'production',
  entry: {
    bottom: join(PATHS.src, 'index.ts'),
  },
  output: {
    path: PATHS.public,
    filename: 'index.js',
    libraryTarget: 'umd',
    library: 'bottom',
  },
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.ts$/,
      loader: 'awesome-typescript-loader',
    }],
  },
  resolve: {
    extensions: ['.ts'],
  },
  optimization: {
    concatenateModules: false,
    minimize: true,
  },
};
