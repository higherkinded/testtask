import {
  areNumbers,
  areStrings,
  isBoolean,
} from '../common/guards'

interface Assoc { [k: string]: {} }

export interface IItem extends Assoc {
  id: number
  title: string
  image: string
  descr: string
  price: number
  available: boolean
}

export const isValidItem = (a: Assoc): a is IItem =>
  areNumbers([a.price, a.id]) &&
  areStrings([a.title, a.image, a.descr]) &&
  isBoolean(a.available)
