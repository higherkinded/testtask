import IAssociative from "../common/IAssociative";
import { IItem } from "../datatypes";
import { StoreSubscriber, Connect } from "../store";

export interface Props extends IAssociative { }

export interface Component<P extends Props> {
  parent?: HTMLElement
  view?: HTMLElement
  lastProps?: P
  attach(parent: HTMLElement): void
  update(props: Partial<P>): void
  render(props: P): HTMLElement
}

export class BaseComponent<P extends Props> implements Component<P> {
  parent?: HTMLElement
  view?: HTMLElement
  lastProps?: P
  connect?: Connect<IAssociative, Props>

  constructor(
    props: P,
    parent?: HTMLElement,
    connect?: Connect<IAssociative, Props>
  ) {
    if (parent) {
      this.attach(parent)
    }
    if (props) {
      this.update(props)
    }
  }

  attach(parent: HTMLElement) {
    this.parent = parent
    if (this.view && this.parent) {
      console.log(3)
      this.parent.appendChild(this.view)
    }
  }

  update(patch: Partial<P>) {
    // Apparently TSC disallows patching with partial
    const newProps = { ...this.lastProps, ...patch } as P
    this.lastProps = newProps
    if (this.parent) {
      console.log(1)
      if (this.view) {
        console.log(2)
        this.parent.removeChild(this.view)
      }
      this.view = this.render(newProps)
      this.attach(this.parent)
    }
  }

  render(_props: P): HTMLElement {
    return document.createElement('div')
  }
}

type CardProps = IItem & {
  addToCard: () => void
}

class Card extends BaseComponent<CardProps> {
  parent?: HTMLElement
  view?: HTMLElement
  lastProps?: CardProps

  render(p: CardProps) {
    this.lastProps = p

    const card = document.createElement('div');
    const button = document.createElement('button');
    button.onclick = p.addToCard

    card.className = 'i-card';
    card.appendChild(button);

    return card;
  }
}

type CardListProps = {
  ids: number[]
}

export class CardList<S extends IAssociative>
  extends BaseComponent<CardListProps> {
  parent?: HTMLElement
  view?: HTMLElement
  lastProps?: CardListProps
  cards: StoreSubscriber<S, CardProps>[]

  render(p: CardListProps, connect: Connect<S, CardProps>) {
    const list = document.createElement('div');
    const cards = p.ids.map(i => new Card(
      {
        id: i,
        title: 'Card',
        image: '',
        descr: '',
        price: 300,
        available: true,
        addToCard: () => { }
      },
      list,
    ))

    this.cards = cards.map(connect(

    ))

    list.className = 'i-list'

    return list
  }
}
