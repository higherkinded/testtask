import IAction from "../common/IAction";
import { bottom } from '@higherkinded/bottom'


type Nullable<A> = A | null


type Lazy<R> = () => R
type Func1<A, R> = (a: A) => R
type Func2<A, B, R> = (a: A, b: B) => R


type Dispatch = Func1<IAction, void>
type Reducer<S> = Func2<IAction, Nullable<Partial<S>>, S>


interface IObservable<A> {
  add: Func1<IObserver<A>, void>
  peek: Lazy<A>
}


interface IObserver<A> {
  notify: Func1<A, void>
  unsubscribe: Lazy<void>
}


interface IPatch<A> {
  patch: Func1<Nullable<A>, void>
}


interface IStateProxy<S> extends IPatch<Partial<S>> {
  peek: Lazy<S>
}


type Queue<A, B> = IObservable<A> & IPatch<B>


type Store<S> = IObservable<S>


const mkState = <S>(intializer: S): IStateProxy<S> => {
  const state = { ...intializer }

  const patch = (patch: Nullable<Partial<S>>) =>
    Object.assign(state, patch) && null

  const peek = () => ({ ...state })

  return { patch, peek }
}


const mkStateDispatcherQueue = <S>(r: Reducer<S>): Queue<S, IAction> => {
  const queue: IAction[] = []
  const subscribers: IObserver<S>[] = []
  let lastState: S
  let dispatching: boolean = false

  const notify = () => subscribers.map(s => s.notify(lastState))

  const processQueue = async () => {
    if (queue.length === 0) {
      // Notify subscriber(s) about the latest state
      notify()
      // Unlock and terminate
      dispatching = false
      return
    }
    dispatching = true
    const nextAction = queue.shift()
    if (nextAction !== undefined) {
      // Reduce, mutate
      Object.assign(lastState, r(nextAction, lastState))
    }
    // Rinse, repeat
    processQueue()
  }

  const patch = (action: Nullable<IAction>) => {
    if (action) {
      queue.push(action)
      if (!dispatching) processQueue()
    }
  }

  const add = (observer: IObserver<S>) => {
    subscribers.push(observer)
    return () => subscribers.splice(subscribers.indexOf(observer), 1) && null
  }

  const peek = () => ({ ...lastState })

  return { patch, peek, add };
}


const createStore = <S>(): Store<S> => {


  return {
    peek: bottom
  }
}
