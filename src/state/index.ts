import { isCartAction, Add, CartAction } from '../actions/cart'
import {
  byName,
  byNone,
  byPrice,
  isFilterAction,
  FilterAction,
  Name,
  None,
  Price,
  ToggleAsc,
} from '../actions/filter'
import { isLoadAction, FinishLoad, StartLoad } from '../actions/load'
import { isPageAction, Next, Prev, SetPage } from '../actions/page'
import IAction from '../common/IAction'
import IAssociative, { IIdMap } from '../common/IAssociative'
import Tag, { Progress, Untouched } from '../common/Tag'
import { IItem } from '../datatypes'

export interface IState extends IAssociative {
  // Contains IDs
  cart: number[]
  goods: {
    asc: boolean
    availableOnly: boolean,
    data: IIdMap<IItem>,
    filtered: IItem[][],
    sortby: Tag
    status: Tag,
  }
  maxPage: number
  page: number
  pageSize: number
}

export const initialState: IState = {
  cart: [],
  goods: {
    asc: true,
    availableOnly: false,
    data: {},
    filtered: [],
    sortby: None,
    status: Untouched,
  },
  maxPage: 1,
  page: 1,
  pageSize: 15,
}

const _toList = (items: IIdMap<IItem>) => Object.keys(items)
  .reduce((acc: IItem[], k) => [...acc, items[parseInt(k, 10)]], [])

const _chunkList = (items: IItem[], size: number) => {
  const result = []
  for (let i = 0; i < Math.floor(items.length / size); i++) {
    result.push(items.slice(i * size, (i + 1) * size))
  }
  return result
}

const _filteredPortions = (
  items: IIdMap<IItem>,
  size: number,
  asc: boolean,
  sortby: Tag,
) => {
  let sort
  switch (sortby) {
    case Name: { sort = byName(asc); break }
    case Price: { sort = byPrice(asc); break }
    default: { sort = byNone(asc); break }
  }
  return _chunkList(_toList(items).sort(sort), size)
}

export const rootReducer = (
  action: IAction,
  state: IState = initialState,
): IState => {
  switch (action.rel) {

    case StartLoad: {
      const { goods, ...other } = state
      return { ...other, goods: { ...goods, status: Progress } }
    }

    case FinishLoad: {
      if (isLoadAction(action) && action.payload.content) {
        const nextState = { ...state }
        const { content } = action.payload
        nextState.goods.data = content
        nextState.maxPage = Math.ceil(
          Object.keys(content).length / state.pageSize,
        )
        nextState.goods.filtered = _chunkList(
          _toList(content),
          nextState.pageSize,
        )
        return nextState
      }
      break
    }

    case SetPage: {
      const nextState = { ...state }
      const { page, maxPage } = nextState
      if (isPageAction(action)) {
        switch (action.payload.kind) {
          case Next: {
            nextState.page = page < maxPage ? page + 1 : page
            return nextState
          }
          case Prev: {
            nextState.page = page > 1 ? page - 1 : page
            return nextState
          }
          case SetPage: {
            const { exact } = action.payload
            if (exact) {
              nextState.page = exact <= maxPage && exact > 0 ?
                exact : page
            }
            return nextState
          }
        }
      }
      break
    }

    case FilterAction: {
      const nextState = { ...state }
      const { goods, pageSize } = nextState
      const { data, asc } = goods
      if (isFilterAction(action)) {
        switch (action.payload) {
          case ToggleAsc: {
            nextState.goods.asc = !asc
            nextState.goods.filtered = _filteredPortions(
              data,
              pageSize,
              nextState.goods.asc,
              nextState.goods.sortby,
            )
            break
          }
          default: {
            nextState.goods.filtered = _filteredPortions(
              data,
              pageSize,
              nextState.goods.asc,
              action.payload,
            )
            break
          }
        }
      }
      return nextState
    }

    case CartAction: {
      const nextState = { ...state }
      if (isCartAction(action) && action.rel === Add) {
        nextState.cart = [...nextState.cart, action.payload.id]
      }
      return nextState
    }

  }
  return state
}
