import { load } from './actions/load'
import { JSON_LOCATION } from './constants/apiEndpoints'
import { LOCAL_STORAGE_KEY } from './constants/keys'
import Store from './store'

import { initialState, rootReducer } from './state'
import { CardList } from './rendering/renderComponent';

window.addEventListener('load', () => {
  localStorage.removeItem(LOCAL_STORAGE_KEY)

  const store = new Store(rootReducer, initialState)
  load(store.dispatch)(JSON_LOCATION)

  const root = document.getElementById('root')
  console.log(root)
  if (root) {
    const List = new CardList({ ids: [1, 2, 3, 4, 5] }, root)
  }
})
