import Tag from './Tag'

export default interface IAction {
  rel: Tag
  payload: {}
}
