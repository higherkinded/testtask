export const patch = <S>(origin: S, patch: Partial<S>): S =>
  ({ ...origin, ...patch })
