export default interface IAssociative { [k: string]: {} }
export interface IIdMap<A> { [k: number]: A }
