export default class Tag { }

export const Success = new Tag()
export const Failure = new Tag()
export const Progress = new Tag()
export const Untouched = new Tag()
