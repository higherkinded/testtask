import IAssociative from './IAssociative'

const _num = typeof 0
const _str = typeof ''
const _bool = typeof false

type Predicate<A> = (a: {}) => a is A

const lProd = (a: boolean, b: boolean) => a && b

const plural = <A>(fn: Predicate<A>) => (a: Array<{}>): a is A[] =>
  a.map(fn).reduce(lProd, true)

export const isNumber = (a: {}): a is number => typeof a === _num
export const isString = (a: {}): a is string => typeof a === _str
export const isBoolean = (a: {}): a is boolean => typeof a === _bool

export const isArray = (a: {}): a is Array<{}> => a instanceof Array

export const isAssoc = (a: {}): a is IAssociative => a instanceof Object
  && typeof Object.keys(a)[0] === 'string' || Object.keys(a).length === 0

export const areAssoc = plural(isAssoc)
export const areArrays = plural(isArray)
export const areNumbers = plural(isNumber)
export const areStrings = plural(isString)
export const areBoolean = plural(isBoolean)
