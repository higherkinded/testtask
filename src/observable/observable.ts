import { IObservable, IObserver } from "./interfaces";
import Tag from "../common/Tag";

export default class Observable<T> implements IObservable<T> {
  private observed: T
  private observers: Map<Tag, IObserver<T>> = new Map()
  constructor(observed: T) {
    this.observed = observed
  }

  update(next: T) {
    this.observed = next
  }

  add(observer: IObserver<T>) {
    this.observers.set(observer.id, observer)
  }

  remove(observer: IObserver<T>) {
    this.observers.delete(observer.id)
  }

  peek() {
    return { ...this.observed }
  }
}
