import { isArray } from '../common/guards'
import IAction from '../common/IAction'
import { IIdMap } from '../common/IAssociative'
import Tag, { Failure, Progress, Success } from '../common/Tag'
import { LOCAL_STORAGE_KEY } from '../constants/keys'
import { isValidItem, IItem } from '../datatypes'
import { Dispatch } from '../store'

export const StartLoad = new Tag
export const FinishLoad = new Tag

export type LoadAction = IAction & {
  payload: {
    content?: IIdMap<IItem>
    status: Tag,
  }
  rel: Tag,
}

export const isLoadAction = (a: IAction): a is LoadAction =>
  a.rel === StartLoad || a.rel === FinishLoad

const startLoad = () => ({ rel: StartLoad, payload: { status: Progress } })

const failLoad = () => ({ rel: FinishLoad, payload: { status: Failure } })

const finishLoad = (content: IIdMap<IItem>) => ({
  payload: {
    content,
    status: Success,
  },
  rel: FinishLoad,
})

const _trySucceed = (dispatch: Dispatch, data: string) => {
  try {
    const parsed = JSON.parse(data)
    if (isArray(parsed)) {
      dispatch(finishLoad(
        parsed.filter(isValidItem)
          .reduce(
            (acc: { [k: number]: IItem }, i: IItem) => ({
              ...acc,
              [i.id]: i,
            }), {},
          ),
      ))
      return true
    }
  } catch (e) {
    dispatch(failLoad())
  }
  return false
}

export const load = (dispatch: Dispatch) =>
  async (path: string): Promise<void> => {
    dispatch(startLoad())
    const local = localStorage.getItem(LOCAL_STORAGE_KEY)

    if (local) {
      _trySucceed(dispatch, local)
      return
    }

    try {
      const remote = await fetch(path)
      const text = await remote.text()
      if (remote.ok && _trySucceed(dispatch, text)) {
        localStorage.setItem(LOCAL_STORAGE_KEY, text)
        return
      }
    } catch (e) {
      dispatch(failLoad())
    }
  }
