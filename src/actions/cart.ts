import IAction from '../common/IAction'
import Tag from '../common/Tag'
import { Dispatch } from '../store'

export const Add = new Tag

export const CartAction = new Tag

export type CartAction = IAction & {
  payload: {
    kind: Tag
    id: number,
  },
}

export const isCartAction = (a: IAction): a is CartAction =>
  a.rel === CartAction

export const addToCart = (d: Dispatch) =>
  (id: number) => () => d({
    rel: CartAction,
    payload: { kind: Add, id },
  })
