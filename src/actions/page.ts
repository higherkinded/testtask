import IAction from '../common/IAction'
import Tag from '../common/Tag'
import { Dispatch } from '../store'

export const SetPage = new Tag

export const Next = new Tag
export const Prev = new Tag
export const GoTo = new Tag

export type SetPage = IAction & {
  payload: {
    kind: Tag
    exact?: number,
  }
  rel: Tag,
}

export const isPageAction = (a: IAction): a is SetPage => a.rel === SetPage

const genAction = (kind: Tag, exact?: number) =>
  ({ rel: SetPage, payload: { kind, exact } })

export const prevPage = (dispatch: Dispatch) =>
  dispatch(genAction(Prev))
export const nextPage = (dispatch: Dispatch) =>
  dispatch(genAction(Next))
export const goToPage = (dispatch: Dispatch, n: number) =>
  dispatch(genAction(GoTo, n))
