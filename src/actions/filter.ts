import IAction from '../common/IAction'
import Tag from '../common/Tag'
import { IItem } from '../datatypes'
import { Dispatch } from '../store'

export const None = new Tag
export const Price = new Tag
export const Name = new Tag
export const ToggleAsc = new Tag
export const ToggleInactive = new Tag
export const FilterAction = new Tag

export type FilterAction = IAction & {
  payload: Tag,
}

export const isFilterAction = (a: IAction): a is FilterAction =>
  a.rel === FilterAction

export const toggleInactive = (d: Dispatch) => () => d({
  rel: FilterAction,
  payload: ToggleInactive,
})

export const sortWith = (d: Dispatch) => (t: Tag) => () => d({
  rel: FilterAction,
  payload: t,
})

export const byPrice = (asc: boolean) => (a: IItem, b: IItem) =>
  (a.price - b.price) * (asc ? 1 : -1)

export const byName = (asc: boolean) => (a: IItem, b: IItem) =>
  Number(asc ? (a.name > b.name) : (a.name < a.name))

export const byNone = (_: boolean) => (_a: IItem, _b: IItem) => 0
